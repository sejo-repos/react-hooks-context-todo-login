import React from "react";
import { NewItem, ItemList } from "./Items";

export default function Todo() {
  return (
    <header className="App-header">
      <h2><span role="img" aria-label="rocket">🚀</span> ToDo App</h2>
      <NewItem />
      <ItemList />
    </header>
  );
}