# React Context+Hooks+Login+ToDo 

- [Referencia](https://dev.to/pubudu/build-a-redux-like-store-with-react-context-hooks-8a6)

## 
    npx create-react-app react-context-hooks-login-todo
    cd react-context-hooks-login-todo
    npm install react-router-dom
    npm start
    
```bash
+---public
|       favicon.ico
|       index.html
|       logo192.png
|       logo512.png
|       manifest.json
|       robots.txt
|       
\---src
     |- api
        |- auth.js
     |- components
        |- Items.js
        |- Login.js
        |- Todos.js
        |- PrivateRoute.js
     |- contexts
        |- AuthContext.js
        |- TodoContext.js
     App.css
     App.js
     Index.js
     serviceWorker.js
     setupTests.js
|   App.css 
|   App.js
|   index.js
|   .gitignore
|   package.json
|   README.md
```
---